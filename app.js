const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const toppings = [{
        name: 'pepperoni',
        price: 20
    },{
        name: 'mushroom',
        price: 40
    }, {
        name: 'cheese',
        price: 0
    }, {
        name: 'pineapple',
        price: 30
    }
]

// Cache topping containers
let $pizza  = null;
let $mushrooms = null;
let $pepperoni = null;
let $pineapple = null;

let $total = null;
let $totalPrice = order.total;

$(document).ready(function () {
    // Write the best jQuery ever here

    $pizza = $('.pizza');
    $total = $('#total');
    
    $total.text($totalPrice);

    $('#pepperoni').click(function(){
        
        if ($pepperoni === null) {
            $pepperoni = $('.pepperonis');
            const generated = generateTopping(toppings[0]);
            $pepperoni.append(generated);
            $cost = toppings[0].price;
        }
        else {
            $pepperoni.toggle();
        }

        if ($pepperoni.is(':hidden')) {
            removePrice(toppings[0]);
        } else {
            addPrice(toppings[0]);
        }
        
    });

    $('#mushroom').click(function(){
        if ($mushrooms === null) {
            $mushrooms = $('.mushrooms');
            const generated = generateTopping(toppings[1]);
            $mushrooms.append(generated);
        }
        else {
            $mushrooms.toggle();
        }

        if ($mushrooms.is(':hidden')) {
            removePrice(toppings[1]);
        } else {
            addPrice(toppings[1]);
        }

    });

    $('#cheese').click(function(){
            $pizza.toggleClass('no-cheese');

            if ($pizza.is(':hidden')) {
                removePrice(toppings[2]);
            } else {
                addPrice(toppings[2]);
            }
    });

    $('#pineapple').click(function(){

        if ($pineapple === null) {
            $pineapple = $('.pineapples');
            const generated = generateTopping(toppings[3]);
            $pineapple.append(generated);
        }
        else {
            $pineapple.toggle();
        }

        if ($pineapple.is(':hidden')) {
            removePrice(toppings[3]);
        } else {
            addPrice(toppings[3]);
        }

    });

    function addPrice(topping){
        $totalPrice += topping.price;
        $total.text($totalPrice);
    }

    function removePrice(topping){
        $totalPrice -= topping.price;
        $total.text($totalPrice);
    }

});




// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping.name);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}

